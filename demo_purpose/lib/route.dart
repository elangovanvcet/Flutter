import 'package:demo_purpose/error.dart';
import 'package:demo_purpose/first.dart';
import 'package:demo_purpose/second.dart';
import 'package:demo_purpose/third.dart';
import 'package:flutter/material.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/first':
        return MaterialPageRoute(builder: (_) => FirstPage());
      case '/second':
        return MaterialPageRoute(builder: (_) => SecondPage());
      case '/third':
        if (settings.arguments.toString() == "manager") {
          return MaterialPageRoute(builder: (_) => ThirdPage());
        } else {
          return MaterialPageRoute(builder: (_) => Error());
        }

      default:
        return MaterialPageRoute(builder: (_) => Error());
    }
  }
}
