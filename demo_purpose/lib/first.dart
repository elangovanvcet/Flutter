import 'package:demo_purpose/second.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class FirstPage extends StatelessWidget {
  FirstPage({Key? key}) : super(key: key);
  var userName = "manager";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        child: Center(
          child: TextButton(
              onPressed: () {
                // Navigator.push(context,
                //     MaterialPageRoute(builder: (context) => SecondPage()));
                Navigator.pushNamed(context, '/second', arguments: userName);
              },
              child: Text("firstpage")),
        ),
      ),
    );
  }
}
