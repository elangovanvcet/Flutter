import 'package:demo_purpose/route.dart';
import 'package:demo_purpose/second.dart';
import 'package:demo_purpose/third.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'first.dart';

void main() {
  runApp(DemoApp());
}

class DemoApp extends StatelessWidget {
  const DemoApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: FirstPage(),
      onGenerateRoute: RouteGenerator.generateRoute,
      initialRoute: "/second",
      // routes: {
      //   '/first': (context) => FirstPage(),
      //   '/second': (context) => SecondPage(),
      //   '/third': (context) => ThirdPage(),
      // },
    );
  }
}
