import 'package:flutter/material.dart';
import 'package:localstorage/pets_buttons_widget.dart';
import 'package:localstorage/user.dart';
import 'package:localstorage/userlocaldata.dart';

import 'button_widget.dart';

class SharedPreferencesPage extends StatefulWidget {
  const SharedPreferencesPage({Key? key}) : super(key: key);

  @override
  State<SharedPreferencesPage> createState() => _SharedPreferencesPageState();
}

class _SharedPreferencesPageState extends State<SharedPreferencesPage> {
  String name = '';
  bool themeColor = true;
  List<String> pets = [];
  @override
  void initState() {
    super.initState();
    name = UserLocalData.getName() ?? '';
    themeColor = UserLocalData.getColor() ?? true;
    pets = UserLocalData.getPets() ?? [];
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          brightness: themeColor ? Brightness.light : Brightness.dark,
        ),
        home: Scaffold(
          appBar: AppBar(
            title: const Text("Local Storage"),
            centerTitle: true,
            actions: [
              IconButton(
                  onPressed: () => () {}, icon: Icon(Icons.nightlight_round)),
              IconButton(
                  onPressed: () => setState(() {
                        themeColor = !themeColor;
                        UserLocalData.setColor(themeColor);
                      }),
                  icon: Icon(themeColor ? Icons.sunny : Icons.nightlight_round))
            ],
          ),
          body: Container(
            child: Column(
              children: [
                buildPets(),
                buildButton(),
                TextFormField(
                  initialValue: name,
                  onChanged: (name) => setState(() {
                    this.name = name;
                  }),
                ),
              ],
            ),
          ),
        ));
  }

  Widget buildButton() => ButtonWidget(
      text: 'Save',
      onClicked: () async {
        await UserLocalData.setPets(pets);
      });
  Widget buildPets() => PetsButtonsWidget(
        pets: pets,
        onSelectedPet: (pet) => setState(
            () => pets.contains(pet) ? pets.remove(pet) : pets.add(pet)),
      );
}
