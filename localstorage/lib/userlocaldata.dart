import 'package:shared_preferences/shared_preferences.dart';

class UserLocalData {
  static late SharedPreferences _preferences;
  static const keyusername = 'username';
  static const keycolor = 'color';
  static const keyList = 'list';
  static const _keyPets = 'pets';
  static Future init() async =>
      _preferences = await SharedPreferences.getInstance();

  static Future setName(String name) async =>
      await _preferences.setString(keyusername, name);

  static Future setColor(bool color) async =>
      await _preferences.setBool(keycolor, color);

  static Future setPets(List<String> pets) async =>
      await _preferences.setStringList(_keyPets, pets);

  static getPets() => _preferences.getStringList(_keyPets);

  static getName() => _preferences.getString(keyusername);

  static getColor() => _preferences.getBool(keycolor);
}
