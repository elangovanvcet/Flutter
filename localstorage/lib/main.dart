import 'package:flutter/material.dart';
import 'package:localstorage/SharedPreferencesPage.dart';
import 'package:localstorage/userlocaldata.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await UserLocalData.init();
  runApp(SharedPreferencesPage());
}
