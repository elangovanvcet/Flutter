import 'package:bloc/controller/EmployeeBloc.dart';
import 'package:bloc/models/employeeModel.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final EmployeeBloc _employeebloc = EmployeeBloc();
  @override
  void dispose() {
    _employeebloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Employee List"),
        ),
        body: Container(
          child: StreamBuilder<List<EmployeeModel>>(
            stream: _employeebloc.employeeListStream,
            builder: (BuildContext context,
                AsyncSnapshot<List<EmployeeModel>> snapshot) {
              return ListView.builder(
                itemCount: snapshot.data?.length,
                itemBuilder: ((context, index) {
                  return Card(
                    elevation: 5.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                          padding: EdgeInsets.all(20),
                          child: Text("${snapshot.data![index].id}",
                              style: TextStyle(fontSize: 20.0)),
                        ),
                        Container(
                          padding: EdgeInsets.all(20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text("${snapshot.data![index].name}",
                                  style: TextStyle(fontSize: 20.0)),
                              Text("${snapshot.data![index].salary}",
                                  style: TextStyle(fontSize: 16.0)),
                            ],
                          ),
                        ),
                        Container(
                          child: IconButton(
                            icon: Icon(Icons.thumb_up),
                            color: Colors.green,
                            onPressed: () => {
                              _employeebloc.employeeIncrementSink
                                  .add(snapshot.data![index])
                            },
                          ),
                        ),
                        Container(
                          child: IconButton(
                            icon: Icon(Icons.thumb_down),
                            color: Colors.red,
                            onPressed: () => {
                              _employeebloc.employeeDecrementSink
                                  .add(snapshot.data![index])
                            },
                          ),
                        ),
                      ],
                    ),
                  );
                }),
              );
            },
          ),
        ));
  }
}
