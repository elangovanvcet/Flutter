import 'dart:async';

import 'package:bloc/models/employeeModel.dart';

class EmployeeBloc {
  final List<EmployeeModel> _employeeList = [
    EmployeeModel(1, "Elan", 199.0),
    EmployeeModel(2, "Alan", 299.0),
    EmployeeModel(3, "Rose", 399.0),
    EmployeeModel(4, "Herlin", 499.0),
    EmployeeModel(5, "Yuva", 599.0),
  ];
  //stream Controller...Sink in...Stream out

  final _employeeListStreamController = StreamController<List<EmployeeModel>>();

  //for incre and decre
  final _employeeSalaryIncrementStreamController =
      StreamController<EmployeeModel>();

  final _employeeSalaryDecrementStreamController =
      StreamController<EmployeeModel>();

  StreamSink<List<EmployeeModel>> get employeeListSink =>
      _employeeListStreamController.sink;

  Stream<List<EmployeeModel>> get employeeListStream =>
      _employeeListStreamController.stream;

  StreamSink<EmployeeModel> get employeeIncrementSink =>
      _employeeSalaryIncrementStreamController.sink;

  StreamSink<EmployeeModel> get employeeDecrementSink =>
      _employeeSalaryDecrementStreamController.sink;

  EmployeeBloc() {
    _employeeListStreamController.add(_employeeList);

    _incrementSalary(EmployeeModel employee) {
      double salary = employee.salary;
      double incrementedSalary = salary * 20 / 100;
      _employeeList[employee.id - 1].salary = salary + incrementedSalary;
      employeeListSink.add(_employeeList);
    }

    _employeeSalaryIncrementStreamController.stream.listen(_incrementSalary);

    _decrementSalary(EmployeeModel employee) {
      double salary = employee.salary;
      double decrementedSalary = salary * 20 / 100;
      _employeeList[employee.id - 1].salary = salary - decrementedSalary;
      employeeListSink.add(_employeeList);
    }

    _employeeSalaryDecrementStreamController.stream.listen(_decrementSalary);
  }

  void dispose() {
    _employeeSalaryIncrementStreamController.close();
    _employeeSalaryDecrementStreamController.close();
    _employeeListStreamController.close();
  }
}
