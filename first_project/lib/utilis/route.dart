import 'package:first_project/utilis/secondPage.dart';
import 'package:first_project/utilis/thirdPage.dart';
import 'package:flutter/material.dart';

import 'error.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case "/second":
        if (settings.arguments.toString() != "manager") {
          return MaterialPageRoute(builder: (_) => SecondPage());
        } else {
          return MaterialPageRoute(builder: (_) => ErrorPage());
        }

      case "/third":
        return MaterialPageRoute(builder: (_) => ThirdPage());
      case "/card":
        return MaterialPageRoute(builder: (_) => Card());
      default:
        return MaterialPageRoute(builder: (_) => ErrorPage());
    }
  }
}
