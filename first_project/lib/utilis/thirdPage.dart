import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/widgets.dart';

class ThirdPage extends StatelessWidget {
  ThirdPage({Key? key}) : super(key: key);
  var assetImage = "assets/images/kiri.jpg";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //  appBar: AppBar(),
      body: ListView(
        scrollDirection: Axis.vertical,
        padding: EdgeInsets.all(20),
        children: [
          CircleAvatar(
              child: ClipOval(
                child: Image.asset(
                  assetImage,
                  width: 250,
                  height: 250,
                  fit: BoxFit.cover,
                ),
              ),
              radius: 70),
          CircleAvatar(
              child: ClipOval(
                child: Image.asset(
                  assetImage,
                  width: 250,
                  height: 250,
                  fit: BoxFit.cover,
                ),
              ),
              radius: 70),
          CircleAvatar(
              child: ClipOval(
                child: Image.asset(
                  assetImage,
                  width: 250,
                  height: 250,
                  fit: BoxFit.cover,
                ),
              ),
              radius: 70),
          CircleAvatar(
              child: ClipOval(
                child: Image.asset(
                  assetImage,
                  width: 250,
                  height: 250,
                  fit: BoxFit.cover,
                ),
              ),
              radius: 70),
          CircleAvatar(
              child: ClipOval(
                child: Image.asset(
                  assetImage,
                  width: 250,
                  height: 250,
                  fit: BoxFit.cover,
                ),
              ),
              radius: 70),
        ],
      ),
    );
  }
}
