import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {
  CardPage({Key? key}) : super(key: key);
  var assetImage = "assets/images/kiri.jpg";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Cards"),
      ),
      body: ListView(
        padding: EdgeInsets.all(18),
        children: [
          Card(
            shadowColor: Colors.red,
            elevation: 8,
            color: Colors.redAccent,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            child: Column(
              children: [
                Text(
                  "nervous weakness like mine is sure to lead to al ",
                  style: TextStyle(fontSize: 24),
                ),
                SizedBox(
                  height: 12,
                ),
                Text(
                  "manner of excited fancies ",
                  style: TextStyle(fontSize: 24),
                )
              ],
            ),
          ),
          SizedBox(
            height: 12,
          ),
          Container(
            padding: EdgeInsets.fromLTRB(20, 20, 20, 0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              gradient: LinearGradient(
                colors: [Colors.blue, Colors.red],
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
              ),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "lead to all manner of excited fancies ",
                  style: TextStyle(fontSize: 24),
                ),
                SizedBox(
                  height: 12,
                ),
                Text(
                  "nervous weakness like mine",
                  style: TextStyle(fontSize: 24),
                )
              ],
            ),
          ),
          SizedBox(
            height: 12,
          ),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
            ),
            child: Stack(
              alignment: Alignment.center,
              children: [
                Container(
                  height: 175,
                  width: 280,
                  decoration: BoxDecoration(
                      color: Colors.blue,
                      image: DecorationImage(
                          image: AssetImage("assets/images/Tree.jpg")),
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.circular(25)),
                ),
                Positioned(
                  bottom: 16,
                  right: 16,
                  left: 16,
                  child: Text(
                    "Avatar way of water",
                    style: TextStyle(color: Colors.white, fontSize: 45),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
