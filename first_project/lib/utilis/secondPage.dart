import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/widgets.dart';

class SecondPage extends StatelessWidget {
  SecondPage({Key? key}) : super(key: key);
  var assetImage = "assets/images/kiri.jpg";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: ListView.builder(
          scrollDirection: Axis.vertical,
          itemCount: 10,
          itemBuilder: (context, int i) {
            return ListTile(
              leading: Icon(Icons.star),
              trailing: Text("ListP$i"),
            );
          }),
    );
  }
}
// GridView.builder(
//           gridDelegate:
//               SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 5),
//           scrollDirection: Axis.horizontal,
//           itemCount: 59,
//           itemBuilder: (context, int i) {
//             return ListTile(
//               leading: Icon(Icons.star),
//               trailing: Text("ListP$i"),
//             );
//           }),