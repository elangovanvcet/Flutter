import 'package:first_project/utilis/secondPage.dart';
import 'package:flutter/material.dart';

//day 7-navigator and routes

class FirstPage extends StatefulWidget {
  const FirstPage({Key? key}) : super(key: key);

  @override
  State<FirstPage> createState() => _FirstPageState();
}

class _FirstPageState extends State<FirstPage> {
  var passvisible = true;
  var emailNametext = TextEditingController();
  var emailPasstext = TextEditingController();
  var assetImage = "assets/images/kiri.jpg";
  var networkImage =
      "https://i.pinimg.com/564x/f7/75/7d/f7757d5977c6ade5ba352ec583fe8e40.jpg";

  //var currentUser = "manager";
  var currentUser = "employee";

  @override
  void initState() {
    super.initState();
    emailNametext.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        centerTitle: true,
        title: Text("Login"),
      ),
      body: Padding(
          padding: EdgeInsets.all(25),
          child: Column(
            children: [
              SizedBox(
                height: 30,
              ),
              profilePicture(),
              SizedBox(
                height: 20,
              ),
              emailName(),
              SizedBox(
                height: 10,
              ),
              emailPass(),
              SizedBox(
                height: 10,
              ),
              submitButton(),
              TextButton(
                  onPressed: () => Navigator.pushNamed(context, "/second"),
                  child: Text("Sign up"))
            ],
          )),
    );
  }

  Widget emailName() {
    return Padding(
      padding: EdgeInsets.all(8),
      child: TextField(
        controller: emailNametext,
        decoration: InputDecoration(
            prefixIcon: Icon(Icons.mail),
            labelText: "Email",
            hintText: "Enter your email id",
            border: OutlineInputBorder(),
            suffixIcon: emailNametext.text.isEmpty
                ? Container(
                    width: 0,
                  )
                : IconButton(
                    icon: Icon(Icons.close),
                    onPressed: () {
                      emailNametext.clear();
                    },
                  )),
      ),
    );
  }

  Widget emailPass() {
    return Padding(
      padding: EdgeInsets.all(8),
      child: TextField(
        controller: emailPasstext,
        decoration: InputDecoration(
            prefixIcon: Icon(Icons.password),
            hintText: "Enter your password",
            labelText: "Password",
            border: OutlineInputBorder(),
            suffixIcon: IconButton(
              icon: passvisible
                  ? Icon(Icons.visibility_off)
                  : Icon(Icons.visibility),
              onPressed: () {
                setState(() {
                  passvisible = !passvisible;
                });
              },
            )),
        obscureText: passvisible,
      ),
    );
  }

  Widget submitButton() {
    return Container(
      width: 100,
      decoration: BoxDecoration(),
      child: ElevatedButton(
          onPressed: () {
            Navigator.pushNamed(context, "/second", arguments: currentUser);
            // Navigator.push(
            //     context, MaterialPageRoute(builder: (context) => SecondPage()));
          },
          child: Text("Submit")),
    );
  }

  Widget profilePicture() {
    return CircleAvatar(
        child: ClipOval(
          child: Image.asset(
            assetImage,
            width: 250,
            height: 250,
            fit: BoxFit.cover,
          ),
        ),
        radius: 70);
    // Image.network(
    //     networkImage,
    //     width: 250,
    //     height: 250,
    //     fit: BoxFit.cover,
    //   ),
    // ),
    // radius: 70);
  }
}
