import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:first_project/utilis/card.dart';
import 'package:first_project/utilis/firstPage.dart';
import 'package:first_project/utilis/route.dart';
import 'package:first_project/utilis/secondPage.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

void main() {
  runApp(const DemoApp());
}

class DemoApp extends StatelessWidget {
  const DemoApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.red,
        brightness: Brightness.light,
      ),
      // routes: {
      //   '/second': (context) => SecondPage(),
      // },
      onGenerateRoute: RouteGenerator.generateRoute,

      // initialRoute: "/second",
      // home: AnimatedSplashScreen(
      //   // splash: Icons.home,
      //   splash: Lottie.asset("assets/json/guitar.json"),
      //   //  splash: Image.asset("assets/images/kiri.jpg"),
      //   duration: 3000,
      //   // nextScreen: FirstPage(),
      //   nextScreen: CardPage(),
      //   backgroundColor: Colors.blue,
      //   curve: Curves.fastLinearToSlowEaseIn,
      //   splashIconSize: 385,
      //   splashTransition: SplashTransition.rotationTransition,
      //   animationDuration: Duration(seconds: 1),
      // ));
      home: CardPage(),
    );
  }
}
