import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:intl/intl.dart';

import '../model/note.dart';

final _lightColors = [
  Colors.blue.shade300,
  Colors.orange.shade300,
  Colors.red.shade300,
  Colors.green.shade300,
  Colors.yellow.shade300,
  Colors.teal.shade300,
];

double getMinHeight(int index) {
  switch (index % 4) {
    case 0:
      return 100;
    case 1:
      return 150;
    case 2:
      return 200;
    case 3:
      return 250;
    default:
      return 100;
  }
}

class NoteCardWidget extends StatelessWidget {
  NoteCardWidget({
    Key? key,
    required this.note,
    required this.index,
  }) : super(key: key);

  final Note note;
  final int index;
  @override
  Widget build(BuildContext context) {
    final time = DateFormat.yMMMd().format(note.createdTime!);
    final color = _lightColors[index % _lightColors.length];
    final height = getMinHeight(index);
    return Card(
      color: color,
      child: Container(
        constraints: BoxConstraints(minHeight: height),
        padding: EdgeInsets.all(8),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              time,
              style: TextStyle(color: Colors.grey.shade800),
            ),
            Text(
              note.title!,
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
            Text(
              note.description!,
              style: TextStyle(
                color: Colors.white,
                fontSize: 10,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
