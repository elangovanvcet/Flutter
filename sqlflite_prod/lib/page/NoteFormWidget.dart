import 'package:flutter/material.dart';

class NoteFormWidget extends StatelessWidget {
  final String? title;
  final String? description;

  final ValueChanged<String> onChangedTitle;
  final ValueChanged<String> onChangedDescription;
  const NoteFormWidget({
    Key? key,
    this.title = '',
    this.description = '',
    required this.onChangedTitle,
    required this.onChangedDescription,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(16),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              buildTitle(),
              SizedBox(
                height: 10,
              ),
              buildDescription(),
            ],
          ),
        ),
      );

  Widget buildTitle() {
    return TextFormField(
      initialValue: title,
      style: TextStyle(
        color: Colors.white,
        fontSize: 24,
        fontWeight: FontWeight.bold,
      ),
      decoration: InputDecoration(
        border: InputBorder.none,
        hintText: 'Title',
        hintStyle: TextStyle(
          color: Colors.white60,
        ),
      ),
      validator: (title) =>
          title != null && title.isEmpty ? 'title cant be empty' : null,
      onChanged: onChangedTitle,
    );
  }

  Widget buildDescription() {
    return TextFormField(
      initialValue: description,
      style: TextStyle(
        color: Colors.white,
        fontSize: 24,
        fontWeight: FontWeight.bold,
      ),
      decoration: InputDecoration(
        border: InputBorder.none,
        hintText: 'Type Something..',
        hintStyle: TextStyle(
          color: Colors.white60,
        ),
      ),
      validator: (title) =>
          title != null && title.isEmpty ? 'description cant be empty' : null,
      onChanged: onChangedDescription,
    );
  }
}
