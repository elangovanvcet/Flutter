import 'package:flutter/material.dart';
import 'package:sqlflite_prod/page/notes_page.dart';

Future main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          primaryColor: Colors.black,
          scaffoldBackgroundColor: Colors.blueGrey.shade900,
          appBarTheme: AppBarTheme(backgroundColor: Colors.transparent)),
      debugShowCheckedModeBanner: false,
      home: Notespage(),
    );
  }
}
