import 'package:shared_preferences/shared_preferences.dart';

class UserLocalData {
  static late SharedPreferences _preferences;
  static Future init() async =>
      _preferences = await SharedPreferences.getInstance();

  static const keyList = 'list';

  static Future setList(List<String> list) async =>
      await _preferences.setStringList(keyList, list);

  static getList() => _preferences.getStringList(keyList);
}
