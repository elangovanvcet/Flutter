import 'package:flutter/material.dart';
import 'package:shared_prealldatatypes/userlocaldata.dart';
import 'SharedPreferencesPage.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await UserLocalData.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Home(),
    );
  }
}
