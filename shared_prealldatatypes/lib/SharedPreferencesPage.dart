import 'package:flutter/material.dart';
import 'package:shared_prealldatatypes/userlocaldata.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  List<String> _lists = [];

  @override
  void initState() {
    super.initState();
    _lists = UserLocalData.getList() ??
        ["1 minute", "2 minute", "3 minute", "4 minute", "5 minute"];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextButton(
            onPressed: () => setState(() {
                  print(_lists);
                  for (int i = 0; i < _lists.length; i++) {
                    _lists[i] = "minute";
                    print(_lists[i]);
                  }
                  UserLocalData.setList(_lists);
                  print(_lists);
                }),
            child: Text(
              "Save",
              style: TextStyle(color: Colors.black),
            )),
      ),
      body: ListView.builder(
          itemCount: _lists.length,
          itemBuilder: (BuildContext context, int index) {
            return Text(_lists[index]);
          }),
    );
  }
}
